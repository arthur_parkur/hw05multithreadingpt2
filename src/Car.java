import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;

public class Car implements Runnable {
    private static int CARS_COUNT;
    private static CyclicBarrier BARRIER;
    private static CyclicBarrier WINNER_BARRIER = new CyclicBarrier(1);

    private static volatile boolean winner = false;

    static Semaphore semaphore;
    static {
        CARS_COUNT = 0;
    }

    private Race race;
    private int speed;
    private CountDownLatch finished;

    private String name;
    public String getName() {
        return name;
    }

    public int getSpeed() {
        return speed;
    }

    public Car(Race race, int speed, CyclicBarrier barrier, Semaphore semaphore, CountDownLatch finished) {
        this.BARRIER = barrier;
        this.semaphore = semaphore;
        this.race = race;
        this.speed = speed;
        this.finished = finished;

        CARS_COUNT++;
        this.name = "Участник #" + CARS_COUNT;
    }
    @Override
    public void run() {
        try {
            System.out.println(this.name + " готовится");
            Thread.sleep(500 + (int)(Math.random() * 800));
            System.out.println(this.name + " готов");
            BARRIER.await();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (int i = 0; i < race.getStages().size(); i++) {
            race.getStages().get(i).go(this);
        }

        if (!winner) {
            try {
                WINNER_BARRIER.await();
            } catch (Exception e) {
                e.printStackTrace();
            }

            winner = true;
            System.out.println(this.getName() + " WIN");
        }

        finished.countDown();
    }

}
